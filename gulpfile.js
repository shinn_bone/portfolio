// var gulp         = require('gulp');
var gulp         = require('gulp-task-doc'); // Instead of require('gulp')
var util         = require('gulp-util');
var browserSync  = require('browser-sync');
var jshint       = require('gulp-jshint');
var rename       = require('gulp-rename');
var sass         = require('gulp-sass');
var uglify       = require('gulp-uglify');
var notify       = require('gulp-notify');
var autoprefixer = require('gulp-autoprefixer');
var include      = require('gulp-include');
var twig         = require('gulp-twig');
var del          = require('del');
var access       = require('gulp-accessibility');
var imagemin     = require('gulp-imagemin');
var imageResize  = require('gulp-image-resize');

// vagrant ssh
var GulpSSH      = require('gulp-ssh');
var os           = require('os');

// required for subcompile
var fs     = require('graceful-fs');
var path   = require('path');
var concat = require('gulp-concat');
var es     = require('event-stream');

// sassfmt
var converter = require('sass-convert');
var cached    = require('gulp-cached');

// misc vars
var autoprefixer_browsers = ['Chrome > 40', 'Edge > 14', 'ie 11', 'Firefox > 40', 'iOS > 9', 'Opera > 40', 'Safari > 9'];

// folder paths
var basePaths = {
  build: './build/',
  mockup: './mockup/',
  production: './'
};

// file paths
var paths = {
  html: {
    build: basePaths.build + 'html/',
    mockup: basePaths.mockup,
  },
  htmlComponents: {
    build: basePaths.build + 'html/components',
  },
  scss: {
    build: basePaths.build + 'scss/',
    mockup: basePaths.mockup + 'css/',
    production: basePaths.production + 'css/'
  },
  js: {
    build: basePaths.build + 'js/',
    mockup: basePaths.mockup + 'js/',
    production: basePaths.production + 'js/'
  },
  twig: {
    production: basePaths.production + 'templates/'
  },
  graphics: {
    build: basePaths.build + 'graphics/',
    mockup: basePaths.mockup + 'graphics/',
    production: basePaths.production + 'graphics/'
  }
};

//##############################################################################
// imagemin
//##############################################################################
/*
Imagemin

usage:
gulp imagemin

description:
optimize and downsize images to < 2000px wide
append .min before ext
directory: images/
filetypes: .jpg, .jpeg, .png, .gif, .svg
*/
gulp.task('imagemin', function() {
  return gulp.src([ 'images/**/{*.jpg,*.jpeg,*.png,*.gif,*.svg}','!images/**/*.min*' ])
    .pipe(imageResize({
      width : 2000,
      upscale : false
    }).on("error", notify.onError(function (error) {
      return "Error - image-resize: " + error.message;
    })))
    .pipe(imagemin()
      .on("error", notify.onError(function (error) {
        return "Error - imagemin: " + error.message;
      })))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('images/min/'));
});


//##############################################################################
// run accessibility tests
//##############################################################################
/*
Accessibility Testing

usage:
gulp test

description:
test .html files for accessibility issues
*/
gulp.task('test', function() {
  return gulp.src(['**/*.html','!node_modules/**/*.*'])
    .pipe(access({
      force: true
    }))
    .on('error', console.log)
    .pipe(access.report({reportType: 'json'}))
    .pipe(rename({
      extname: '.json'
    }))
    .pipe(gulp.dest('tests/'));
});


//##############################################################################
// help
//##############################################################################
/*
Display this help
*/
gulp.task('help', gulp.help());


//##############################################################################
// sassfmt
//##############################################################################
/*
use sass-convert to format scss files
*/

gulp.task('sassfmt', function () {
  return gulp.src(paths.scss.build + '**/*.scss')
    .pipe(cached('fmt').on("error", notify.onError(function (error) {
      return "Error - cached fmt: " + error.message;
    })))
    .pipe(converter({
      from: 'scss',
      to: 'scss',
    }).on("error", notify.onError(function (error) {
      return "Error - SCSS converter: " + error.message + " " + __filename;
    })))
    .pipe(gulp.dest(paths.scss.build))
    .pipe(notify({message: "formatting file: <%= file.relative %>"}));
});


//##############################################################################
// sass
//##############################################################################
/*
Compiles sass, autoprefixes, and outputs to mockups and production
*/
gulp.task('sass', ['sassfmt'], function() {
  return gulp.src(paths.scss.build + 'stylesheet.scss')
    .pipe(sass({outputStyle: 'compressed'})
      .on("error", notify.onError(function (error) {
        return "Error - Sass: " + error.message;
      })))
    .pipe(autoprefixer(autoprefixer_browsers))
    .pipe(gulp.dest(paths.scss.mockup))
    .pipe(gulp.dest(paths.scss.production))
    .pipe(browserSync.stream({}))
    .pipe(notify({ message: "sass file: <%= file.relative %>"}));
});


//##############################################################################
// html
//##############################################################################
/*
Twig for html mockups

Source files:
 *.html files are compiled
 *.twig files are compiled

Output files:
mockups/
*/
gulp.task('html', function() {
  return gulp.src([paths.html.build + '**/*.{html,twig}', '!' + paths.html.build + '**/_*.{html,twig}'])
    .pipe(
      twig({
        data: {
          root: '.',
          build:  './build',
        }
      })
    )
    .on("error", notify.onError(function (error) {
      return "Error - HTML: " + error.message;
    }))
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest(paths.html.mockup))
    .pipe(browserSync.stream({}))
    .pipe(notify({ message: "html file: <%= file.relative %>"}));
});

//##############################################################################
// js
//##############################################################################
/*
Runs lint on site.js and components/*.js
renames site.js to site.inc.js and saves to mockups
minifies site.js to production
*/
gulp.task('js', function(){
  var lint = gulp.src([paths.js.build + 'site.js', paths.js.build + 'components/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));

  var js = gulp.src([paths.js.build + 'site.js'])
    .pipe(include().on("error", notify.onError(function (error) {
      return "Error - js include: " + error.message;
    })))
    .pipe(rename({suffix: '.inc'}))
    .pipe(gulp.dest(paths.js.mockup))
    .pipe(uglify().on("error", notify.onError(function (error) {
      return "Error - uglify: " + error.message;
    })))
    .pipe(rename({basename: 'site.min'},{suffix: ''}))
    .pipe(gulp.dest(paths.js.production))
    .pipe(notify({ message: "js file: <%= file.relative %>"}));

  return lint,js;
});


//##############################################################################
// js-watch
//##############################################################################
/*
Run js task and then reload browserSync
*/
gulp.task('js-watch', ['js'], function (done) {
  browserSync.reload();
  done();
});


//##############################################################################
// subcompile
//##############################################################################
var scriptsPath = './subdesigns';

// log path
var logFile = function(es) {
  return es.map(function(file, cb) {
    util.log(file.path);
    return cb();
  });
};

// get all folders for subcompile
function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}

/*
Iteratively compile sass and autoprefix all subdesign folders
*/
gulp.task('subcompile', function() {
  var folders = getFolders(scriptsPath);

  var tasks = folders.map(function(folder) {
    return gulp.src(path.join(scriptsPath, folder, '/scss/stylesheet.scss'), { style: 'compressed' })
      .pipe(sass({outputStyle: 'compressed'})
        .on("error", notify.onError(function (error) {
          return "Error - Sass: " + error.message;
        })))
      .pipe(autoprefixer(autoprefixer_browsers))
      .pipe(gulp.dest(path.join(scriptsPath, folder, '/css')))
      .pipe(logFile(es))
      .pipe(notify({ message: "subcompile file: <%= file.relative %>"}));
  });

  return es.concat.apply(null, tasks);

});


//##############################################################################
// browser sync
//##############################################################################
/*
Launch a browser sync session
*/

// browserSync for local mockups
gulp.task('browser-sync', ['prod'], function() {
  browserSync.init({
    open: false,
    notify: false,
    tunnel: true,
    server: {
      baseDir: "./",
      index: "./",
      directory: true
    }
  });
});

//##############################################################################
// prod
//##############################################################################
/*
html | sass | js | imagemin | vagrant
*/
gulp.task('prod', ['sass','html','js-watch','imagemin'], function(){
});


//##############################################################################
// default gulp
//##############################################################################
/*
Watches for changes to files in ./build/ and run their respective compilers
*/

// watch - local/mockups
gulp.task('default', ['browser-sync'], function() {
  gulp.watch(paths.scss.build + '**/*.scss', ['sass']);
  gulp.watch(paths.html.build + '**/*.{html,twig}', ['html'],  browserSync.reload);
  gulp.watch(paths.js.build + '**/*.js', ['js-watch'], browserSync.reload);
  gulp.watch('./images/*', ['imagemin']);

  gulp.watch(['./subdesigns/**/*', '!./subdesigns/**/*.css'], ['subcompile']);
});


