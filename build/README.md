# Design Documentation

## Design Name - design_path

#### Functionality
* here is where you would list all unique design functionality
* things like "news article description have a 200 character cutoff"


#### Navigation
* here is where you'll list all navigation information.
* things like "primary nav is sticky", "internal subnav is 'accordion'", and "portal nav will display when ___ happens"


#### Recommendations and Guidelines
* here is where you will list any recommendations or guidelines
* things like "featured stories works best with only three collection items"


#### Logos
* here is where you'll list where the three header select options will render:
* header = 1
* footer = 2
* landing = 3>1


#### Blockspaces
* here is where you will list out all of your content block block spaces and what should go in them.
* examples below:
* additional-header-block - generic, not styled.
* additional-footer-block - generic, not styled.
* donate-block - collection, label only.
* social-media-block - collection - images only - homepage topnav, header, white images by default.
* powered-by-block - text - footer.
* logos-block - collection - images only.
* fast-facts-block - collection (4).

#### Content Spaces (custom only)

##### Homepage
* List homepage layout content spaces

##### Internal
 * List internal layout content spaces

## After Mockup Changes
* here is where you will list any changes that were made post mockup.
* things like "mobile navigation is a slightly different color than PDS's"
