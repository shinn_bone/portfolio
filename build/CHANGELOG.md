changelog
---------------------------------------------
##gulp3

# Fri May 11 10:49:37 CDT 2018
* add gulp imagemin
* add gulp test for accessibility testing
* add gulp watch in main 'default' tasks that runs subcompile on any subdesigns change
* move slider__nav outside of collection--list
* move autoprefixer browsers into a variable
* remove outdated gulp tasks "savesite, savetheme"
* convert all comments to `//##`
* update sass to output compressed
* remove gulp devdeps that are no longer needed
* update homepage internal to remedy commented twig issue
* add <p> to news article descriptions
* update navChecker version
* add textlimit js
* add class to header element
* add keyboardNavigation.js
* center landing page logo
* center landing social media block
* update counter.js to better handle strings

# 1/29/2018
* update print/_quicklinks.html partial
* add general/_location_standard.html partial
* add general/_calendar.html and general/_calendar_list.html partial
* add general/catalog_express.html and general/_image_caption.html partial
