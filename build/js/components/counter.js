//##############################################################################
// counter.js
//
// Description:
//
// Usage:
// counter(container_element, element_to_be_counted_up, speed);
//
// Restrictions:
// will strip text inbetween 2 numbers
// will concate 2 numbers separated by text
//##############################################################################
function initCounter() {
  $('.fast-facts-container .collection-item-description p').each(function() {
    var $this = $(this);

    var original_html = $this.html();
    $this.attr('data-original-html', original_html);

    var prefix = original_html.match(/^\D+/g);
    $this.attr('data-prefix', prefix);

    var suffix = original_html.match(/\D+$/g);
    $this.attr('data-suffix', suffix);

    var number = String(original_html.match(/\d+/g)).replace(/,\s?/g, "");

    $this.attr('data-numeric', number);

    console.log(prefix +":"+ number +":"+ suffix);

    if ($this.html().indexOf(',') != -1) {
      $this.attr('data-comma', true);
    }
    if ($this.html().indexOf('.') != -1) {
      $this.attr('data-decimal', true);
    }

  });
}
initCounter();
function counter(container, counter, speed) {
  container.each(function() {
    counter.each(function() {
      var $this = $(this);
      var data = $this.attr('data-numeric');
      var prefix = $this.attr('data-prefix') != null ? $this.attr('data-prefix') : '';
      var suffix = $this.attr('data-suffix') != null ? $this.attr('data-suffix') : '';
      var hasDecimal = $this.attr('data-decimal') != null ? '.' : '';
      var hasCommas = $this.attr('data-comma') != null ? true : false;

      $({value: 0}).animate({
        value: data
      }, {
        duration: speed,
        easing: 'swing',
        step: function(now, fx) {
          if (data != 'null'){
            var num = Math.round(now);
            if ( hasDecimal ) {
              num = (num/100).toFixed(2);
            }
            if ( hasCommas ) {
              num = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            }
            $this.html( prefix + num + suffix);
          }
        }
      });

    });
  });
}
