//##############################################################################
// accordionMenu.js
//
// Description:
// applies accordion functionality to any nav with class .accordion
//##############################################################################
$(document).ready(function() {

  if( $('nav.accordion').length > 0 ) {

    $('nav.accordion').find('ul').children('li').has('ul').each(function() {
      $(this).children('a').append('<span class="accordion_toggle"></span>');
      if( $(this).hasClass('nav__list--here') ) {
        $(this).addClass('accordion_open');
        $(this).closest('li').children('ul').slideDown();
      }
    });

    $('span.accordion_toggle').click(function(n) {
      n.preventDefault();
      if (!$(this).closest('li').hasClass('accordion_open')) {
        $(this).closest('li').siblings().removeClass('accordion_open').children('ul').slideUp();
        $(this).closest('li').addClass('accordion_open');
        $(this).closest('li').children('ul').slideDown();
      } else {
        $(this).closest('li').removeClass('accordion_open').children('ul').slideUp();
      }
    });

  }

});
