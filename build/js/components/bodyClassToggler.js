//##############################################################################
// bodyClassToggler.js
//
// Description:
// function that takes an element and applies a class when clicked
//
// Usage:
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'add');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'remove');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'toggle');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible');
//
// note: 'toggle' is default
// the following 2 examples yeild the same results
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'toggle');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible');
//##############################################################################
function bodyClassToggler(element, elemClass, condition){
  $(element).on('click', function(){
    switch(condition){
      case 'add':
        $('body').addClass(elemClass);
        break;

      case 'remove':
        $('body').removeClass(elemClass);
        break;

      default:
        $('body').toggleClass(elemClass);
        break;
    }
  });
}
