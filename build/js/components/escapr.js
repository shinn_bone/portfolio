//##############################################################################
// Escapr.js
//
// Description:
// when the escape key is pressed:
// any <targetElement> element will have its <targetClass> class removed
//
// Usage:
// escapr('body.class-is-visible', 'class-is-visible'));
//##############################################################################
function escapr(targetElement, targetClass) {
  $(document).keyup(function(e) {
    if (e.keyCode == 27) {
      $(targetElement).removeClass(targetClass);
    }
  });
}
