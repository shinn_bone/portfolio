//##############################################################################
// Focusr.js
//
// Description:
// when any <triggerElement> element is clicked <targetElement> is gived focus
//
// Usage:
// focusr($('.search-trigger'), $('.search-input input'));
//##############################################################################
function focusr(triggerElement, targetElement) {
  triggerElement.on('click', function(){
    targetElement.focus();
  });
}
