//##############################################################################
// Keyboard navigation in dropdown menus
//##############################################################################
$(document).ready(function() {

  // TODO:  move this to site.js
  // Setup the keyboard nav
  $('.nav-keyboard').keyboardNavigation();

});

// keycodes for search jumping
var keyCodeMap = {
  48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
  65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
  77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
  96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9"
};

//##############################################################################
// setup navigation
//##############################################################################
$.fn.keyboardNavigation = function(settings) {

  settings = jQuery.extend({
    menuHoverClass: 'show-menu',
  }, settings);

  var debug = false;

  // Add ARIA role to menubar and menu items
  $(this).attr('role', 'menubar').find('li').attr('role', 'menuitem');

  // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
  $(this).find('a').next('ul')
    .attr('data-test','true')
    .attr({ 'aria-hidden': 'true', 'role': 'menu' })
    .find('a')
    .attr('tabIndex',-1);

  // Adding aria-haspopup for appropriate items
  $(this).find('a').each(function(){
    if($(this).next('ul').length > 0)
      $(this).parent('li').attr('aria-haspopup', 'true');
  });

  $(this).find('a').hover(function(){
    $(this).closest('ul')
      .attr('aria-hidden', 'false')
      .find('.'+settings.menuHoverClass)
      .attr('aria-hidden', 'true')
      .removeClass(settings.menuHoverClass)
      .find('a')
      .attr('tabIndex',-1);
    $(this).next('ul')
      .attr('aria-hidden', 'false')
      .addClass(settings.menuHoverClass)
      .find('a').attr('tabIndex',0);
  });

  $(this).find('a').focus(function(){
    $(this).closest('ul')
      .find('.'+settings.menuHoverClass)
      .attr('aria-hidden', 'true')
      .removeClass(settings.menuHoverClass)
      .find('a')
      .attr('tabIndex',-1);

    $(this).next('ul')
      .attr('aria-hidden', 'false')
      .addClass(settings.menuHoverClass)
      .find('a').attr('tabIndex',0);
  });

  //##############################################################################
  // Bind keys for navigation
  // left/right, up/down, escape, enter/space, other - searching
  //##############################################################################
  var currentLevel = 0;

  $(this).find("a").keydown(function(e){

    var isParent = false;

    if ($(this).parent('li').hasClass('nav-level-0')) { currentLevel = 0;}
    else if ($(this).parent('li').hasClass('nav-level-1')) { currentLevel = 1; }
    else if ($(this).parent('li').hasClass('nav-level-2')) { currentLevel = 2; }
    else if ($(this).parent('li').hasClass('nav-level-3')) { currentLevel = 3; }
    else if ($(this).parent('li').hasClass('nav-level-4')) { currentLevel = 4; }
    else if ($(this).parent('li').hasClass('nav-level-5')) { currentLevel = 5; }

    if ($(this).parent('li').hasClass('nav__list--parent')) {
      isParent = true;
    } else {
      isParent = false;
    }

    if (debug) {
      console.log(isParent);
    }

    switch ( e.keyCode ) {


        //##############################################################################
        // LEFT ARROW
        //##############################################################################
      case 37:
        e.preventDefault();
        if (debug) {
          console.log('left');
        }

        if (currentLevel == 0) {
          $(this).parent('li').prev('li').find('>a').focus();
        } else {
          // go up a level
          $(this).parent('li').parents('li').find('>a').focus();
        }

        break;

        //##############################################################################
        // UP ARROW
        //##############################################################################
      case 38:
        e.preventDefault();
        if (debug) {
          console.log('up');
        }

        if (currentLevel == 0 ) {
        } else if (currentLevel == 1) {

          // if its the top li in a dropdown go up a level
          if ($(this).parent('li').prev('li').length == 0) {
            if (debug) {
              console.log('top item in dropdown');
            }
            $(this).parent('li').parents('li').find('>a').focus();
          }
          else {
            $(this).parent('li').prev('li').find('>a').focus();
          }

        } else {
          $(this).parent('li').prev('li').find('>a').focus();
        }
        break;

        //##############################################################################
        // RIGHT ARROW
        //##############################################################################
      case 39:
        e.preventDefault();
        if (debug) {
          console.log('right');
        }

        if (currentLevel == 0 ) {
          $(this).parent('li').next('li').find('>a').focus();
        } else {

          if (isParent) {
            $(this).next('ul').find('>li').first().find('>a').focus();
          }

        }

        break;

        //##############################################################################
        // DOWN ARROW
        //##############################################################################
      case  40:
        e.preventDefault();
        if (debug) {
          console.log('down');
        }

        if ( currentLevel == 0 ) {

          if (isParent) {
            $(this).next('ul').find('>li').first().find('>a').focus();
          }

        } else {
          $(this).parent('li').next('li').find('>a').focus();
        }


        break;



        //##############################################################################
        // ENTER
        // SPACE
        //##############################################################################
      case 13:
      case 32:
        e.preventDefault();
        if (debug) {
          console.log('enter or space');
        }
        window.location = $(this).attr('href');
        break;

        //##############################################################################
        // ESCAPE
        //##############################################################################
      case 27:
        e.preventDefault();
        if (debug) {
          console.log('escape');
        }


        $(this)
          .parents('ul').first()
          .prev('a').focus()
          .parents('ul').first().find('.'+settings.menuHoverClass)
          .attr('aria-hidden', 'true')
          .removeClass(settings.menuHoverClass)
          .find('a')
          .attr('tabIndex',-1);

        break;

        //##############################################################################
        // OTHER - search for first letter
        //##############################################################################
      default:


        if (currentLevel == 0 ) {
          $(this).parent('li').find('ul[aria-hidden=false] a').each(function(){
            if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
              $(this).focus();
              return false;
            }
          });
        } else {
          var found = false;
          $(this).parent('li').nextAll('li').find('a').each(function(){
            if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
              $(this).focus();
              found = true;
              return false;
            }
          });

          if(!found) {
            $(this).parent('li').prevAll('li').find('a').each(function(){
              if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
                $(this).focus();
                return false;
              }
            });
          }
        }

        break;
    }


  });



  var timer = null;

  // If the user tabs out of the navigation hide all menus
  $(this).find('a').last().keydown(function(e){
    if(e.keyCode == 9) {
      $('.'+settings.menuHoverClass)
        .attr('aria-hidden', 'true')
        .removeClass(settings.menuHoverClass)
        .find('a')
        .attr('tabIndex',-1);
    }
  });

  // Hide menu if click or focus occurs outside of navigation
  $(document).click(function(){
    $('.'+settings.menuHoverClass)
      .attr('aria-hidden', 'true')
      .removeClass(settings.menuHoverClass)
      .find('a')
      .attr('tabIndex',-1);
  });

  // start timer to clear nav
  $(this).on('mouseout mouseleave', function(){
    clearTimeout(timer);
    timer = setTimeout(function(){
      if (debug) {
        console.log(timer);
      }
      $('.'+settings.menuHoverClass)
        .attr('aria-hidden', 'true')
        .removeClass(settings.menuHoverClass)
        .find('a')
        .attr('tabIndex',-1);
    }, 1000);
  });

  // reset timer
  $(this).on('mousein mouseenter', function(){
    if (debug) {
      console.log(timer);
    }
    clearTimeout(timer);
  });

  $(this).click(function(e){
    e.stopPropagation();
  });

};


