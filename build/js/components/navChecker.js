//##############################################################################
// navChecker.js (1.0.2)
// 2018 Tyler Fowle
//
// Description:
// check the widths of given child elements against the width of a container
// add a class when children exceed container
//
// Options:
// debug: boolean, if true: enables console logs and other debugging options
// activeClass: the class that is added to 'targets', default "desktop-nav-is-too-wide"
// children: array of jquery elements to calc widths, defaults to all direct children
// targets: array of jquery elements that 'activeClass' is applied
// minWidth: minimum width of window, if window is less than this number the activeClass will be applied
//
// Usage:
// $('header .wrap').navChecker({
//   debug: true,
//   activeClass: 'added-class',
//   children: [$('nav.dropdown > ul > li')],
//   targets: [$('body'), $('.search-block')],
//   minWidth: 1024
// });
//
// Default settings:
// $('header .wrap').navChecker();
//##############################################################################
(function ($, window, document, undefinded){
  var pluginName = 'navChecker';

  function NavChecker(el, options, sel) {
    this.$el = $(el);
    this.selector = sel;
    var defaults = {
      debug: false,
      activeClass: 'desktop-nav-is-too-wide',
      children: [],
      childrenWidth: 0,
      targets: [$('body')],
      minWidth: 0
    };
    this.options = $.extend({}, defaults, options);
    this.init();
  }

  NavChecker.prototype = {

    init: function () {
      var plugin = this;
      var customChildren = true;

      if (plugin.options.children.length === 0) {
        plugin.options.children.push(plugin.$el.children());
        customChildren = false;
        plugin.defaultStates();
      }

      plugin.initEvents();

      $(window).load(function() {
        plugin.createBreakpoint(customChildren);
        plugin.checkSize();
      });
    },

    createBreakpoint: function(customChildren) {
      var plugin = this;
      var start, finish;


      if (plugin.options.debug) {
        start = (new Date()).getTime();
      }

      // get the inner width of each child
      $.each(plugin.options.children, function (index, child) {
        child.each(function () {
          if (customChildren == true) {
            if (plugin.options.debug) {
              console.log($(this).attr("class") + ": " + $(this).innerWidth());
            }
            plugin.options.childrenWidth += $(this).innerWidth();
          } else {
            if ($(this).attr('data-state') == "visible") {
              if (plugin.options.debug) {
                console.log($(this).attr("class") + ": " + $(this).innerWidth());
              }
              plugin.options.childrenWidth += $(this).innerWidth();
            }
          }

          if (plugin.options.debug==true) {
            console.log(plugin.options.childrenWidth);
            $(this).css("border", "1px solid red");
          }

        });
      });


      // set the width of the container to 0 to squash all the margin autos
      // then get the widths of all the remaining margins
      plugin.$el.css('visibility', 'hidden').width(0);

      $.each(plugin.options.children, function (index, child) {
        child.each(function () {
          var margin = 0;

          if (customChildren == true) {
            margin = parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
            if (plugin.options.debug) {
              console.log($(this).attr("class") + ": " + $(this).css('margin-left') + $(this).css('margin-right'));
            }
            plugin.options.childrenWidth += margin;
          } else {
            if ($(this).attr('data-state') == "visible") {
              margin = parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
              if (plugin.options.debug) {
                console.log($(this).attr("class") + ": " + $(this).css('margin-left') + $(this).css('margin-right'));
              }
              plugin.options.childrenWidth += margin;
            }
          }

          if (plugin.options.debug==true) {
            console.log(plugin.options.childrenWidth);
            $(this).css("border", "1px solid red");
          }

        });
      });

      plugin.$el.css('visibility', 'visible').width('');
      if (plugin.options.debug) {
        finish = (new Date()).getTime();
        console.log(finish - start + "ms");
      }
      plugin.options.childrenWidth += parseInt(plugin.$el.css('padding-left')) + parseInt(plugin.$el.css('padding-right'));
      plugin.$el.data('data-breakpoint', plugin.options.childrenWidth);

    },

    initEvents: function () {
      var plugin = this;
      $(window).on('resize load ready', function(){
        plugin.checkSize();
      });
    },

    checkSize: function(){
      var plugin = this;

      if (plugin.options.debug) {
        console.log($(window).width() + ">=" + plugin.$el.data('data-breakpoint'));
      }

      if ($(window).width() > plugin.options.minWidth) {
        if ($(window).width() <= plugin.$el.data('data-breakpoint') ) {
          plugin.updateClasses('add');
        } else {
          plugin.updateClasses('remove');
        }
      } else {
        plugin.updateClasses('add');
      }

    },

    defaultStates: function() {
      var plugin = this;
      $.each(plugin.options.children, function (index, child) {
        child.each(function(){


          $(this).attr("data-width", $(this).outerWidth());
          if ($(this).is(':visible')) {
            $(this).attr('data-state','visible');
          } else {
            $(this).attr('data-state','hidden');
          }

        });
      });

    },

    updateClasses: function (operation) {
      var plugin = this;

      $.each(plugin.options.targets, function (index, target) {
        if (operation == 'remove') {
          target.removeClass(plugin.options.activeClass);
        } else {
          target.addClass(plugin.options.activeClass);
        }
      });

    }

  };

  $.fn[pluginName] = function (options) {
    var sel = this.selector;
    return this.each(function () {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new NavChecker(this, options, sel));
      }
    });
  };

})(jQuery, window, document);
