//##############################################################################
// Imports
//##############################################################################

// = include components/updatevalue.js
// = include components/backgroundImage.js
// = include components/bodyClassToggler.js
// = include components/placeholders.js
// = include components/accordionMenu.js
// = include components/navChecker.js
// = include components/textWrapper.js
// = include components/fireslider_settings.js
// = include components/counter.js
// = include components/landing_page.js
// = include components/escapr.js
// = include components/focusr.js
// = include components/keyboardNavigation.js


$(window).load(function(){ 
$("onload-ani").css('background', 'linear-gradient(to right, red 50%, blue 50%);');
});

$(document).ready(function() {
	
setTimeout(
  function() 
  {
$(".onload-ani-white, .onload-ani-white, .onload-ani-clear").css({"background-position": "left bottom"});
  }, 0);

$(".global-nav").click(function(){
	$(".global-nav").toggleClass('active');
});

	
  // Body Class Toggler
  //##############################################################################
  // bodyClassToggler('.element-trigger', 'element-is-visible');

  // Fast Facts Counter
  //##############################################################################
/*  if ($('.fast-facts-container').length > 0 && $(window).width() > 674 ) {*/
    //var triggered = false;
    //$(function() {
      //$(window).bind('scroll load', function(){
        //var featTop = $('.end-fast-facts').offset().top - $(window).innerHeight();
        //var bodyTop = $(window).scrollTop();
        //if (triggered === false) {
          //if( bodyTop > featTop ){
            //triggered = true;
            //counter($('.fast-facts-container'), $('.collection-item-description p') , 1000);
          //}
        //}
      //});
    //});
  //} else {
    //counter($('.fast-facts-container'), $('.collection-item-description p') , 0);
  /*}*/

}); // end document ready

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          }
        });
      }
    }
  });


/* Every time the window is scrolled ... */
$(window).scroll( function(){

    /* Check the location of each desired element */
    $('.hideme').each( function(i){

        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it in */
        if( bottom_of_window > bottom_of_object ){

            $(this).animate({'opacity':'1','top' : '0'},500);

        }

    }); 

    /* Check the location of each desired element */
    $('.hideme-horizontal').each( function(i){

        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it in */
        if( bottom_of_window > bottom_of_object ){

            $(this).animate({'opacity':'1','left' : '0'},500);

        }

    }); 

});
