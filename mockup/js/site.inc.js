//##############################################################################
// Imports
//##############################################################################

//##############################################################################
// updateValue.js
//
// Description:
// Function that updates the value field of text inputs
//
// Usage:
// updateValue(".search-container input", "Search")
//##############################################################################
function updateValue(input, text) {
  input.val(text);

  if(input.val() === null) {
    input.val(text);
  }
  input.focus(function() {
    if(input.val() === text) {
      input.val("");
    }
  });
  input.blur(function() {
    if(input.val() === null || input.val() === "") {
      input.val(text);
    }
  });
}

//##############################################################################
// Background Image
// Function that applies the first image to the background of the element with options
//
// element: class to target
// options: background options
// hide: 1 for <img> only, 2 for entire .image div
//
// examples:
// backgroundImage($(".has-bg"), "center center / cover no-repeat", 2);
// backgroundImage($(".has-bg__collection li"), "center center / cover no-repeat", 2);
// backgroundImage($(".has-bg-li__collection li .collection-item-image"), "center center / cover no-repeat", 1);
//##############################################################################
function backgroundImage(element, options, hide) {
  $( element ).each(function() {
    var imgSrc = $(this).find('img').first().attr('src');

    if (imgSrc) {
      if ( hide == 1 ) {
        $(this).find('img').first('img').hide();
      }
      else {
        $(this).find('img').parent('.image, .spotlight-image').hide();
      }
      $(this).css('background', 'url('+imgSrc+')' + options);
    }

  });
}

//##############################################################################
// bodyClassToggler.js
//
// Description:
// function that takes an element and applies a class when clicked
//
// Usage:
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'add');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'remove');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'toggle');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible');
//
// note: 'toggle' is default
// the following 2 examples yeild the same results
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible', 'toggle');
// bodyClassToggler('mobile_menu', 'mobile_menu_is_visible');
//##############################################################################
function bodyClassToggler(element, elemClass, condition){
  $(element).on('click', function(){
    switch(condition){
      case 'add':
        $('body').addClass(elemClass);
        break;

      case 'remove':
        $('body').removeClass(elemClass);
        break;

      default:
        $('body').toggleClass(elemClass);
        break;
    }
  });
}

//##############################################################################
// placeholders.js
//
// Description:
// Placeholders function that puts the label as a placeholder in input type text, textarea, and option
// To use replace 'form' with whatever you want to target, 'form' will do all forms on site:
//
// Usage:
// placeHolders('form');
//##############################################################################
function placeHolders(form) {

  $(form).find('.portal-login div, .form-row, .e2ma_signup_form_row').each(function() {

    //if form has class
    if($(form).hasClass('fdpc_designready_order_form')) {
      return;
    }

    //if 'this' has class
    if($(this).hasClass('form-row--sub-options')) {
      return;
    }
    if($(this).hasClass('form-row--file')) {
      return;
    }
    if($(this).hasClass('form-row--date')) {
      return;
    }
    if($(this).hasClass('form-row--datetime')) {
      return;
    }
    if($(this).hasClass('form-row--time')) {
      return;
    }
    if($(this).hasClass('payment--cc-exp')) {
      return;
    }

    //if any parents have classes
    if ($(this).parents('.checkout_process, .event-calendar-search__jump, .catalogs--pageflex').length) {
      return;
    }

    var label = $(this).find('.form-row__label label, .e2ma_signup_form_label');
    var input = $(this).find('.form-row__controls input[type="text"], .form-row__controls input[type="email"], .form-row__controls input[type="password"], .e2ma_signup_form_element input[type="text"], .e2ma_signup_form_element input[type="email"]');
    var textarea = $(this).find('textarea');
    var text = $.trim(label.text()).replace(/ +(?= )/g,'');
    var isRequired = false;

    if($(this).hasClass('form-row--required')) {
      isRequired = true;
    }

    if(isRequired) {
      text = text + ' *';
    }

    if (!text) {
      return;
    }

    if(input.length) {
      $(input).attr("placeholder", text);
      label.hide();
      $(this).find('.form-row__label').hide();
    }

    if(textarea.length) {
      $(textarea).attr("placeholder", text);
      label.hide();
      $(this).find('.form-row__label').hide();
    }

    //show things that should be showing
    if( $(this).hasClass('payment--cc-csc') ){
      $(this).find('.form-row__label').show();
    }

  });
}

//##############################################################################
// accordionMenu.js
//
// Description:
// applies accordion functionality to any nav with class .accordion
//##############################################################################
$(document).ready(function() {

  if( $('nav.accordion').length > 0 ) {

    $('nav.accordion').find('ul').children('li').has('ul').each(function() {
      $(this).children('a').append('<span class="accordion_toggle"></span>');
      if( $(this).hasClass('nav__list--here') ) {
        $(this).addClass('accordion_open');
        $(this).closest('li').children('ul').slideDown();
      }
    });

    $('span.accordion_toggle').click(function(n) {
      n.preventDefault();
      if (!$(this).closest('li').hasClass('accordion_open')) {
        $(this).closest('li').siblings().removeClass('accordion_open').children('ul').slideUp();
        $(this).closest('li').addClass('accordion_open');
        $(this).closest('li').children('ul').slideDown();
      } else {
        $(this).closest('li').removeClass('accordion_open').children('ul').slideUp();
      }
    });

  }

});

//##############################################################################
// navChecker.js (1.0.2)
// 2018 Tyler Fowle
//
// Description:
// check the widths of given child elements against the width of a container
// add a class when children exceed container
//
// Options:
// debug: boolean, if true: enables console logs and other debugging options
// activeClass: the class that is added to 'targets', default "desktop-nav-is-too-wide"
// children: array of jquery elements to calc widths, defaults to all direct children
// targets: array of jquery elements that 'activeClass' is applied
// minWidth: minimum width of window, if window is less than this number the activeClass will be applied
//
// Usage:
// $('header .wrap').navChecker({
//   debug: true,
//   activeClass: 'added-class',
//   children: [$('nav.dropdown > ul > li')],
//   targets: [$('body'), $('.search-block')],
//   minWidth: 1024
// });
//
// Default settings:
// $('header .wrap').navChecker();
//##############################################################################
(function ($, window, document, undefinded){
  var pluginName = 'navChecker';

  function NavChecker(el, options, sel) {
    this.$el = $(el);
    this.selector = sel;
    var defaults = {
      debug: false,
      activeClass: 'desktop-nav-is-too-wide',
      children: [],
      childrenWidth: 0,
      targets: [$('body')],
      minWidth: 0
    };
    this.options = $.extend({}, defaults, options);
    this.init();
  }

  NavChecker.prototype = {

    init: function () {
      var plugin = this;
      var customChildren = true;

      if (plugin.options.children.length === 0) {
        plugin.options.children.push(plugin.$el.children());
        customChildren = false;
        plugin.defaultStates();
      }

      plugin.initEvents();

      $(window).load(function() {
        plugin.createBreakpoint(customChildren);
        plugin.checkSize();
      });
    },

    createBreakpoint: function(customChildren) {
      var plugin = this;
      var start, finish;


      if (plugin.options.debug) {
        start = (new Date()).getTime();
      }

      // get the inner width of each child
      $.each(plugin.options.children, function (index, child) {
        child.each(function () {
          if (customChildren == true) {
            if (plugin.options.debug) {
              console.log($(this).attr("class") + ": " + $(this).innerWidth());
            }
            plugin.options.childrenWidth += $(this).innerWidth();
          } else {
            if ($(this).attr('data-state') == "visible") {
              if (plugin.options.debug) {
                console.log($(this).attr("class") + ": " + $(this).innerWidth());
              }
              plugin.options.childrenWidth += $(this).innerWidth();
            }
          }

          if (plugin.options.debug==true) {
            console.log(plugin.options.childrenWidth);
            $(this).css("border", "1px solid red");
          }

        });
      });


      // set the width of the container to 0 to squash all the margin autos
      // then get the widths of all the remaining margins
      plugin.$el.css('visibility', 'hidden').width(0);

      $.each(plugin.options.children, function (index, child) {
        child.each(function () {
          var margin = 0;

          if (customChildren == true) {
            margin = parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
            if (plugin.options.debug) {
              console.log($(this).attr("class") + ": " + $(this).css('margin-left') + $(this).css('margin-right'));
            }
            plugin.options.childrenWidth += margin;
          } else {
            if ($(this).attr('data-state') == "visible") {
              margin = parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
              if (plugin.options.debug) {
                console.log($(this).attr("class") + ": " + $(this).css('margin-left') + $(this).css('margin-right'));
              }
              plugin.options.childrenWidth += margin;
            }
          }

          if (plugin.options.debug==true) {
            console.log(plugin.options.childrenWidth);
            $(this).css("border", "1px solid red");
          }

        });
      });

      plugin.$el.css('visibility', 'visible').width('');
      if (plugin.options.debug) {
        finish = (new Date()).getTime();
        console.log(finish - start + "ms");
      }
      plugin.options.childrenWidth += parseInt(plugin.$el.css('padding-left')) + parseInt(plugin.$el.css('padding-right'));
      plugin.$el.data('data-breakpoint', plugin.options.childrenWidth);

    },

    initEvents: function () {
      var plugin = this;
      $(window).on('resize load ready', function(){
        plugin.checkSize();
      });
    },

    checkSize: function(){
      var plugin = this;

      if (plugin.options.debug) {
        console.log($(window).width() + ">=" + plugin.$el.data('data-breakpoint'));
      }

      if ($(window).width() > plugin.options.minWidth) {
        if ($(window).width() <= plugin.$el.data('data-breakpoint') ) {
          plugin.updateClasses('add');
        } else {
          plugin.updateClasses('remove');
        }
      } else {
        plugin.updateClasses('add');
      }

    },

    defaultStates: function() {
      var plugin = this;
      $.each(plugin.options.children, function (index, child) {
        child.each(function(){


          $(this).attr("data-width", $(this).outerWidth());
          if ($(this).is(':visible')) {
            $(this).attr('data-state','visible');
          } else {
            $(this).attr('data-state','hidden');
          }

        });
      });

    },

    updateClasses: function (operation) {
      var plugin = this;

      $.each(plugin.options.targets, function (index, target) {
        if (operation == 'remove') {
          target.removeClass(plugin.options.activeClass);
        } else {
          target.addClass(plugin.options.activeClass);
        }
      });

    }

  };

  $.fn[pluginName] = function (options) {
    var sel = this.selector;
    return this.each(function () {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new NavChecker(this, options, sel));
      }
    });
  };

})(jQuery, window, document);

//##############################################################################
// textWrapper.js
//
// Description:
// wraps text in div with class name
//
// Usage:
// textWrapper($(".testimonial-container"), ".collection-item-description", "*", "collection-item-quote");
//##############################################################################
function textWrapper(targetContainer, target, separator, wrapClass ) {
  $(targetContainer).each(function() {
    $(this).find(target).html(function (i, html) {
      if ( $(this).text().indexOf(separator) >= 0 ){
        splitText = $(this).text().split(separator);
        formattedText =  splitText[0] + '<span class="'+wrapClass+'">' + splitText[1] + "</span>" ;
        $(this).html(formattedText);
      }
    });
  });
}

//##############################################################################
// Fireslider Settings
//##############################################################################

//-------------------------------------
//  carousel
//-------------------------------------
// $(".js-slider--carousel .js-slider__contents > ul").each(function(){
//   $(this).fireSlider({
//     delay:7500,
//     disableLinks:false,
//     show:3,
//     active:2,
//     effect:"fadeInOut",
//     activeSlideClass:"slide--active",
//     activePagerClass:"slider__pager--active",
//     breakpoints:sliderCarouselBreakpoints,
//     pager:$(this).parents(".slider__contents").siblings(".slider__pager"),
//     prev:$(this).parents(".slider__contents").siblings(".slider__nav").find(".slider-nav--prev"),
//     next:$(this).parents(".slider__contents").siblings(".slider__nav").find(".slider-nav--next")
//   })
// });


//-------------------------------------
//  NO carousel
//-------------------------------------
// $(".js-slider--no-carousel .js-slider__contents > ul").each(function(){
//   $(this).fireSlider({
//     delay:7500,
//     disableLinks:false,
//     show:1,
//     active:1,
//     effect:"fadeInOut",
//     activeSlideClass:"slide--active",
//     activePagerClass:"slider__pager--active",
//     pager:$(this).parents(".slider__contents").siblings(".slider__pager"),
//     prev:$(this).parents(".slider__contents").siblings(".slider__nav").find(".slider-nav--prev"),
//     next:$(this).parents(".slider__contents").siblings(".slider__nav").find(".slider-nav--next")
//   })
// });


//-------------------------------------
//  breakpoints example
//-------------------------------------
// var bps = [
//     {breakpoint: 1, show: 1, active: 1},
//     {breakpoint: 640, show: 2, active: 1},
//     {breakpoint: 1000, show: 4, active: 2}
// ];



// Spotlight Slider
if ( $('.spotlight-container ul > li').length > 1 ) {
  $('.spotlight-container ul').each(function() {
    $(this).fireSlider({
      delay: 8000,
      hoverPause: true,
      pager: $('.spotlight-container .slider-controls-pager'),
      prev:$(this).parents(".slider__contents").siblings(".slider__controls").find(".slider-nav--prev"),
      next:$(this).parents(".slider__contents").siblings(".slider__controls").find(".slider-nav--next")
    });
  });
}



// Logos Slider
var bps = [
  {breakpoint: 1, show: 1, active: 1},
  {breakpoint: 640, show: 2, active: 1},
  {breakpoint: 800, show: 3, active: 1},
  {breakpoint: 1000, show: 4, active: 2}
];

if ( $('.logos-container .collection--list ul > li').length > 1 ) {
  $('.logos-container .collection--list ul').fireSlider({
    active: 2,
    delay: 8000,
    hoverPause: true,
    disableLinks: false,
    prev: $(".logos-container").find(".slider-nav--prev"),
    next: $(".logos-container").find(".slider-nav--next"),
    breakpoints: bps
  });
}

//##############################################################################
// counter.js
//
// Description:
//
// Usage:
// counter(container_element, element_to_be_counted_up, speed);
//
// Restrictions:
// will strip text inbetween 2 numbers
// will concate 2 numbers separated by text
//##############################################################################
function initCounter() {
  $('.fast-facts-container .collection-item-description p').each(function() {
    var $this = $(this);

    var original_html = $this.html();
    $this.attr('data-original-html', original_html);

    var prefix = original_html.match(/^\D+/g);
    $this.attr('data-prefix', prefix);

    var suffix = original_html.match(/\D+$/g);
    $this.attr('data-suffix', suffix);

    var number = String(original_html.match(/\d+/g)).replace(/,\s?/g, "");

    $this.attr('data-numeric', number);

    console.log(prefix +":"+ number +":"+ suffix);

    if ($this.html().indexOf(',') != -1) {
      $this.attr('data-comma', true);
    }
    if ($this.html().indexOf('.') != -1) {
      $this.attr('data-decimal', true);
    }

  });
}
initCounter();
function counter(container, counter, speed) {
  container.each(function() {
    counter.each(function() {
      var $this = $(this);
      var data = $this.attr('data-numeric');
      var prefix = $this.attr('data-prefix') != null ? $this.attr('data-prefix') : '';
      var suffix = $this.attr('data-suffix') != null ? $this.attr('data-suffix') : '';
      var hasDecimal = $this.attr('data-decimal') != null ? '.' : '';
      var hasCommas = $this.attr('data-comma') != null ? true : false;

      $({value: 0}).animate({
        value: data
      }, {
        duration: speed,
        easing: 'swing',
        step: function(now, fx) {
          if (data != 'null'){
            var num = Math.round(now);
            if ( hasDecimal ) {
              num = (num/100).toFixed(2);
            }
            if ( hasCommas ) {
              num = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            }
            $this.html( prefix + num + suffix);
          }
        }
      });

    });
  });
}

function landingImage(element, options) {
  $( element ).each(function() {
    var imgSrc = $(this).find('.masthead-container img').first().attr('src');
    $(this).find('.masthead-container').hide();
    $(this).css('background', 'url('+imgSrc+')' + options);
  });
}

$(document).ready(function() {
  landingImage($(".has-bg-landing"), "center center / cover no-repeat");
});

//##############################################################################
// Escapr.js
//
// Description:
// when the escape key is pressed:
// any <targetElement> element will have its <targetClass> class removed
//
// Usage:
// escapr('body.class-is-visible', 'class-is-visible'));
//##############################################################################
function escapr(targetElement, targetClass) {
  $(document).keyup(function(e) {
    if (e.keyCode == 27) {
      $(targetElement).removeClass(targetClass);
    }
  });
}

//##############################################################################
// Focusr.js
//
// Description:
// when any <triggerElement> element is clicked <targetElement> is gived focus
//
// Usage:
// focusr($('.search-trigger'), $('.search-input input'));
//##############################################################################
function focusr(triggerElement, targetElement) {
  triggerElement.on('click', function(){
    targetElement.focus();
  });
}

//##############################################################################
// Keyboard navigation in dropdown menus
//##############################################################################
$(document).ready(function() {

  // TODO:  move this to site.js
  // Setup the keyboard nav
  $('.nav-keyboard').keyboardNavigation();

});

// keycodes for search jumping
var keyCodeMap = {
  48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
  65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
  77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
  96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9"
};

//##############################################################################
// setup navigation
//##############################################################################
$.fn.keyboardNavigation = function(settings) {

  settings = jQuery.extend({
    menuHoverClass: 'show-menu',
  }, settings);

  var debug = false;

  // Add ARIA role to menubar and menu items
  $(this).attr('role', 'menubar').find('li').attr('role', 'menuitem');

  // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
  $(this).find('a').next('ul')
    .attr('data-test','true')
    .attr({ 'aria-hidden': 'true', 'role': 'menu' })
    .find('a')
    .attr('tabIndex',-1);

  // Adding aria-haspopup for appropriate items
  $(this).find('a').each(function(){
    if($(this).next('ul').length > 0)
      $(this).parent('li').attr('aria-haspopup', 'true');
  });

  $(this).find('a').hover(function(){
    $(this).closest('ul')
      .attr('aria-hidden', 'false')
      .find('.'+settings.menuHoverClass)
      .attr('aria-hidden', 'true')
      .removeClass(settings.menuHoverClass)
      .find('a')
      .attr('tabIndex',-1);
    $(this).next('ul')
      .attr('aria-hidden', 'false')
      .addClass(settings.menuHoverClass)
      .find('a').attr('tabIndex',0);
  });

  $(this).find('a').focus(function(){
    $(this).closest('ul')
      .find('.'+settings.menuHoverClass)
      .attr('aria-hidden', 'true')
      .removeClass(settings.menuHoverClass)
      .find('a')
      .attr('tabIndex',-1);

    $(this).next('ul')
      .attr('aria-hidden', 'false')
      .addClass(settings.menuHoverClass)
      .find('a').attr('tabIndex',0);
  });

  //##############################################################################
  // Bind keys for navigation
  // left/right, up/down, escape, enter/space, other - searching
  //##############################################################################
  var currentLevel = 0;

  $(this).find("a").keydown(function(e){

    var isParent = false;

    if ($(this).parent('li').hasClass('nav-level-0')) { currentLevel = 0;}
    else if ($(this).parent('li').hasClass('nav-level-1')) { currentLevel = 1; }
    else if ($(this).parent('li').hasClass('nav-level-2')) { currentLevel = 2; }
    else if ($(this).parent('li').hasClass('nav-level-3')) { currentLevel = 3; }
    else if ($(this).parent('li').hasClass('nav-level-4')) { currentLevel = 4; }
    else if ($(this).parent('li').hasClass('nav-level-5')) { currentLevel = 5; }

    if ($(this).parent('li').hasClass('nav__list--parent')) {
      isParent = true;
    } else {
      isParent = false;
    }

    if (debug) {
      console.log(isParent);
    }

    switch ( e.keyCode ) {


        //##############################################################################
        // LEFT ARROW
        //##############################################################################
      case 37:
        e.preventDefault();
        if (debug) {
          console.log('left');
        }

        if (currentLevel == 0) {
          $(this).parent('li').prev('li').find('>a').focus();
        } else {
          // go up a level
          $(this).parent('li').parents('li').find('>a').focus();
        }

        break;

        //##############################################################################
        // UP ARROW
        //##############################################################################
      case 38:
        e.preventDefault();
        if (debug) {
          console.log('up');
        }

        if (currentLevel == 0 ) {
        } else if (currentLevel == 1) {

          // if its the top li in a dropdown go up a level
          if ($(this).parent('li').prev('li').length == 0) {
            if (debug) {
              console.log('top item in dropdown');
            }
            $(this).parent('li').parents('li').find('>a').focus();
          }
          else {
            $(this).parent('li').prev('li').find('>a').focus();
          }

        } else {
          $(this).parent('li').prev('li').find('>a').focus();
        }
        break;

        //##############################################################################
        // RIGHT ARROW
        //##############################################################################
      case 39:
        e.preventDefault();
        if (debug) {
          console.log('right');
        }

        if (currentLevel == 0 ) {
          $(this).parent('li').next('li').find('>a').focus();
        } else {

          if (isParent) {
            $(this).next('ul').find('>li').first().find('>a').focus();
          }

        }

        break;

        //##############################################################################
        // DOWN ARROW
        //##############################################################################
      case  40:
        e.preventDefault();
        if (debug) {
          console.log('down');
        }

        if ( currentLevel == 0 ) {

          if (isParent) {
            $(this).next('ul').find('>li').first().find('>a').focus();
          }

        } else {
          $(this).parent('li').next('li').find('>a').focus();
        }


        break;



        //##############################################################################
        // ENTER
        // SPACE
        //##############################################################################
      case 13:
      case 32:
        e.preventDefault();
        if (debug) {
          console.log('enter or space');
        }
        window.location = $(this).attr('href');
        break;

        //##############################################################################
        // ESCAPE
        //##############################################################################
      case 27:
        e.preventDefault();
        if (debug) {
          console.log('escape');
        }


        $(this)
          .parents('ul').first()
          .prev('a').focus()
          .parents('ul').first().find('.'+settings.menuHoverClass)
          .attr('aria-hidden', 'true')
          .removeClass(settings.menuHoverClass)
          .find('a')
          .attr('tabIndex',-1);

        break;

        //##############################################################################
        // OTHER - search for first letter
        //##############################################################################
      default:


        if (currentLevel == 0 ) {
          $(this).parent('li').find('ul[aria-hidden=false] a').each(function(){
            if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
              $(this).focus();
              return false;
            }
          });
        } else {
          var found = false;
          $(this).parent('li').nextAll('li').find('a').each(function(){
            if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
              $(this).focus();
              found = true;
              return false;
            }
          });

          if(!found) {
            $(this).parent('li').prevAll('li').find('a').each(function(){
              if($(this).text().substring(0,1).toLowerCase() == keyCodeMap[e.keyCode]) {
                $(this).focus();
                return false;
              }
            });
          }
        }

        break;
    }


  });



  var timer = null;

  // If the user tabs out of the navigation hide all menus
  $(this).find('a').last().keydown(function(e){
    if(e.keyCode == 9) {
      $('.'+settings.menuHoverClass)
        .attr('aria-hidden', 'true')
        .removeClass(settings.menuHoverClass)
        .find('a')
        .attr('tabIndex',-1);
    }
  });

  // Hide menu if click or focus occurs outside of navigation
  $(document).click(function(){
    $('.'+settings.menuHoverClass)
      .attr('aria-hidden', 'true')
      .removeClass(settings.menuHoverClass)
      .find('a')
      .attr('tabIndex',-1);
  });

  // start timer to clear nav
  $(this).on('mouseout mouseleave', function(){
    clearTimeout(timer);
    timer = setTimeout(function(){
      if (debug) {
        console.log(timer);
      }
      $('.'+settings.menuHoverClass)
        .attr('aria-hidden', 'true')
        .removeClass(settings.menuHoverClass)
        .find('a')
        .attr('tabIndex',-1);
    }, 1000);
  });

  // reset timer
  $(this).on('mousein mouseenter', function(){
    if (debug) {
      console.log(timer);
    }
    clearTimeout(timer);
  });

  $(this).click(function(e){
    e.stopPropagation();
  });

};





$(window).load(function(){ 
$("onload-ani").css('background', 'linear-gradient(to right, red 50%, blue 50%);');
});

$(document).ready(function() {
	
setTimeout(
  function() 
  {
$(".onload-ani-white, .onload-ani-white, .onload-ani-clear").css({"background-position": "left bottom"});
  }, 0);

$(".global-nav").click(function(){
	$(".global-nav").toggleClass('active');
});

	
  // Body Class Toggler
  //##############################################################################
  // bodyClassToggler('.element-trigger', 'element-is-visible');

  // Fast Facts Counter
  //##############################################################################
/*  if ($('.fast-facts-container').length > 0 && $(window).width() > 674 ) {*/
    //var triggered = false;
    //$(function() {
      //$(window).bind('scroll load', function(){
        //var featTop = $('.end-fast-facts').offset().top - $(window).innerHeight();
        //var bodyTop = $(window).scrollTop();
        //if (triggered === false) {
          //if( bodyTop > featTop ){
            //triggered = true;
            //counter($('.fast-facts-container'), $('.collection-item-description p') , 1000);
          //}
        //}
      //});
    //});
  //} else {
    //counter($('.fast-facts-container'), $('.collection-item-description p') , 0);
  /*}*/

}); // end document ready

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          }
        });
      }
    }
  });


/* Every time the window is scrolled ... */
$(window).scroll( function(){

    /* Check the location of each desired element */
    $('.hideme').each( function(i){

        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it in */
        if( bottom_of_window > bottom_of_object ){

            $(this).animate({'opacity':'1','top' : '0'},500);

        }

    }); 

    /* Check the location of each desired element */
    $('.hideme-horizontal').each( function(i){

        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it in */
        if( bottom_of_window > bottom_of_object ){

            $(this).animate({'opacity':'1','left' : '0'},500);

        }

    }); 

});
